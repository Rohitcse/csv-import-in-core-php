<!Doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Import & Export CSV Files</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>
  <style>  
body  
{  
background-image:url("https://images.pexels.com/photos/1166643/pexels-photo-1166643.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500.jpg"); 
 background-size: cover;
}  
</style>  
<div class="container" style="margin-top:50px">
<h1 style="text-align:center;">Import & Export CSV Files</h1><br>
  <div class="row">
    <!--- this form is for the Import data--->
    <form action="import_data.php" method="POST" enctype="multipart/form-data" style="margin-left: 500px;">
      <div class="form-group">
        <label>Select File</label>
        <input type="file" name="file" class="form-control" required="">  
      </div>
      <div class="form-group">
        <input type="submit" name="import" class="btn btn-primary" value="Upload">  
      </div>
    </form>  
    
    <!--- This form is for the Export data--->
    <form action="export_data.php" method="POST" style="margin-left:1010px;margin-bottom: 20px;">
      <input type="submit" class="btn btn-success" name="export" value="Export to Excel">
    </form>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
        </tr>
      </thead>
      <?php
          // include database connectivity 

          include_once('config.php');

          // fetch data from users table 

          $query  = "SELECT * FROM users";
          $result = mysqli_query($con, $query);
          if (mysqli_num_rows($result) > 0) {
            while ($data = mysqli_fetch_assoc($result)) {
          ?>
          <tr>
            <td><?php echo $data['id']?></td>
            <td><?php echo $data['firstname']?></td>
            <td><?php echo $data['lastname']?></td>
            <td><?php echo $data['email']?></td>
          </tr>
        <?php } } ?> 
    </table>
  </div>
</body>
</html>
