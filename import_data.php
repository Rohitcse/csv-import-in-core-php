<?php

	// include database connectivity 

    include_once('config.php');

    // Import file in php

    if (isset($_POST['import'])) {
    	
    	$fileName = $_FILES["file"]["tmp_name"];

    	if ($_FILES['file']['size'] > 0) {

    		$file = fopen($fileName, "r");

            while (($importData = fgetcsv($file, 10000, ",")) !== FALSE) {

                $firstname = "";
                if (isset($importData[0])) {
                    $firstname = mysqli_real_escape_string($con, $importData[0]);
                }
                $lastname = "";
                if (isset($importData[0])) {
                    $lastname = mysqli_real_escape_string($con, $importData[1]);
                }
                $email = "";
                if (isset($importData[1])) {
                    $email = mysqli_real_escape_string($con, $importData[2]);
                }

                $query = "INSERT INTO users (firstname, lastname, email) 
                VALUES('".$firstname."','".$lastname."','".$email."')";
                
                $result = mysqli_query($con, $query);

                if (!isset($result)) {
                    echo "<script type=\"text/javascript\">
                              alert(\"Invalid File:Please Upload CSV File.\");
                              window.location = \"index.php\"
                          </script>";
                }else{
                    echo "<script type=\"text/javascript\">
                              alert(\"CSV File has been successfully Imported.\");
                              window.location = \"index.php\"
                          </script>";
                }
            }
            fclose($file);
        }
    }

?>